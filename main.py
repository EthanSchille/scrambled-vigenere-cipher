import os
import re
from tkinter import filedialog
from math import sqrt
# A quick, brown fox jumped over the lazy dog.
# MJUDYPTCOKABEFHIGNQXVWRLSZ
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def print_vigenere_table(custom_alphabet):
    """
    Prints out the Vigenère table determined by the user's custom alphabet
    :param custom_alphabet: A set of all alphabetical characters from A-Z in any order
    """
    first_line = "\n\n  | "
    for i in range(len(custom_alphabet)):
        first_line += custom_alphabet[i] + " "
    print(first_line)
    print("---------------------------------------------------------")
    for shift in range(len(custom_alphabet)):
        line = custom_alphabet[shift] + " | "
        for index in range(len(custom_alphabet)):
            shifted_index = (index + shift) % len(custom_alphabet)
            line += custom_alphabet[shifted_index] + " "
        print(line)
    print("\n\n")


def encrypt_plaintext(plaintext, key, alphabet):
    """
    Encrypts the user's plaintext using whatever alphabet and key they determine.
    :param plaintext: The user's plaintext
    :param key: An alphabetical key longer than 2 letters long
    :param alphabet: A set of all alphabetical characters from A-Z in any order
    :return: The encrypted text
    """
    # Print the Vigenère table
    print_vigenere_table(alphabet)

    ciphertext = ""
    key_index = 0

    # Loop through every character in the plaintext
    for char in plaintext:
        # For each character in the plaintext, if it's in the alphabet (i.e., not numbers, spaces, punctuation
        # marks, etc.), find the corresponding encrypted character
        if char.upper() in alphabet:
            # The shift is the index of the key's current character
            shift = alphabet.index(key[key_index].upper())
            # The index of the current plaintext character in the alphabet
            plaintext_index = alphabet.index(char.upper())
            # Add the plaintext index and the shift amount, mod it by 26 to get the index of the encrypted
            # character
            encrypted_char = alphabet[(plaintext_index + shift) % 26]
            # add the encrypted character to the ciphertext string
            ciphertext += encrypted_char

            # Increment the key index - if you reach the end, the mod operator will loop the index back around to
            # the beginning of the key
            key_index = (key_index + 1) % len(key)
        else:
            ciphertext += char

    return ciphertext


def decrypt_ciphertext(ciphertext, key, alphabet):
    """
    Decrypts the ciphertext using whatever alphabet and key has been determined.
    :param ciphertext: The ciphertext
    :param key: An alphabetical key longer than 2 letters long
    :param alphabet: A set of all alphabetical characters from A-Z in any order
    :return: The decrypted plaintext
    """
    plaintext = ""
    key_index = 0

    # Loop through every character in the ciphertext
    for char in ciphertext:
        # For each character in the ciphertext, if it's in the alphabet (i.e., not numbers, spaces, punctuation
        # marks, etc.), find the corresponding decrypted character
        if char.upper() in alphabet:
            # The shift is the index of the key's current character
            shift = alphabet.index(key[key_index].upper())
            # The index of the current ciphertext character in the alphabet
            ciphertext_index = alphabet.index(char.upper())
            # Subtract the shift amount from the ciphertext index, mod it by 26 to get the index of the decrypted
            # character
            decrypted_char_index = (ciphertext_index - shift) % len(alphabet)
            decrypted_char = alphabet[decrypted_char_index]
            # Append the decrypted character to the plaintext
            plaintext += decrypted_char

            # Increment the key index - if you reach the end, the mod operator will loop the index back around to
            # the beginning of the key
            key_index = (key_index + 1) % len(key)
        else:
            plaintext += char

    return plaintext


def index_of_coincidence(ciphertext):
    alphabet = ALPHABET
    counts = [0]*26
    for char in ciphertext:
        if char in alphabet:
            counts[alphabet.index(char)] += 1
    # print(counts)
    numer = 0
    total = 0
    for i in range(26):
        numer += counts[i]*(counts[i]-1)
        total += counts[i]
    return 26*numer / (total*(total-1))


def find_key_length(ciphertext):
    found = False
    period = 0
    ciphertext = re.sub(r'[^a-zA-Z]+', '', ciphertext)
    while not found:
        period += 1
        slices = ['']*period
        for i in range(len(ciphertext)):
            slices[i % period] += ciphertext[i]
        sum = 0
        for i in range(period):
            sum += index_of_coincidence(slices[i])
        ioc = sum / period
        # print(period, ioc)
        if ioc > 1.6:
            found = True
    # print(period)
    return period


def cosine_angle(x, y):
    numerator = 0
    length_xx = 0
    length_yy = 0
    for i in range(len(x)):
        numerator += x[i]*y[i]
        length_xx += x[i]*x[i]
        length_yy += y[i]*y[i]
    return numerator / sqrt(length_xx*length_yy)


def get_mono_frequencies_table():
    length = 0
    text = ''
    with open(os.path.join('monofrequency trainer', 'mere-christianity.txt'), 'r', encoding='utf8') as mc:
        temp_text = mc.read()
        temp_text = re.sub(r'[^a-zA-Z]+', '', temp_text)
        text += temp_text
        length += len(temp_text)
    with open(os.path.join('monofrequency trainer', 'pride-and-prejudice.txt'), 'r', encoding='utf8') as pap:
        temp_text = pap.read()
        temp_text = re.sub(r'[^a-zA-Z]+', '', temp_text)
        text += temp_text
        length += len(temp_text)
    with open(os.path.join('monofrequency trainer', 'the-adventures-of-sherlock-holmes.txt'), 'r', encoding='utf8') as sh:
        temp_text = sh.read()
        temp_text = re.sub(r'[^a-zA-Z]+', '', temp_text)
        text += temp_text
        length += len(temp_text)
    mono_frequencies = [0] * 26
    text = text.upper()
    for char in text:
        if char in ALPHABET:
            x = ALPHABET.index(char)
            mono_frequencies[x] += 1
    for i in range(26):
        mono_frequencies[i] = mono_frequencies[i] / len(text)
    return mono_frequencies


def organize_frequencies(frequencies, alphabet):
    true_frequencies = []
    for char in ALPHABET:
        true_frequencies.append(frequencies[alphabet.index(char)])
    return true_frequencies


def guess_frequencies(frequencies):
    # print(frequencies)
    mono_frequencies = get_mono_frequencies_table()
    mono_freq_dict = [{'letter': ALPHABET[i], 'freq': mono_frequencies[i]} for i in range(26)]
    mono_freq_dict.sort(key=lambda x: x['freq'], reverse=True)
    freq_dict = [{'letter': ALPHABET[i], 'freq': frequencies[i]} for i in range(26)]
    freq_dict.sort(key=lambda x: x['freq'], reverse=True)
    one_to_one = [{'letter': mono_freq_dict[i]["letter"], 'result': freq_dict[i]["letter"], 'value': freq_dict[i]['freq']} for i in range(26)]
    one_to_one.sort(key=lambda x: x['letter'])
    # print(one_to_one)
    my_alphabet = ''
    my_freq = []
    for i in range(26):
        my_alphabet += one_to_one[i]['result']
        my_freq.append(one_to_one[i]['value'])
    # print(my_alphabet)
    return my_alphabet, my_freq


def crack_ciphertext(ciphertext, alphabet):
    period = find_key_length(ciphertext)
    ct_alpha = re.sub(r'[^a-zA-Z]+', '', ciphertext)
    mono_frequencies = get_mono_frequencies_table()
    frequencies = []
    guessed_alphabet = ALPHABET
    slices = ['']*period
    for i in range(len(ct_alpha)):
        slices[i % period] += ct_alpha[i]
    for i in range(period):
        frequencies.append([0]*26)
        for j in range(len(slices[i])):
            # frequencies[i][alphabet.index(slices[i][j])] += 1
            frequencies[i][ALPHABET.index(slices[i][j])] += 1
        for j in range(26):
            frequencies[i][j] = frequencies[i][j] / len(slices[i])
    key = ['A']*period
    for i in range(period):
        guessed_alphabet, temp_freq = guess_frequencies(frequencies[i])
        for j in range(26):
            testtable = temp_freq[j:]+temp_freq[:j]
            if cosine_angle(mono_frequencies, testtable) > 0.9:
                key[i] = guessed_alphabet[j]
                print(guessed_alphabet)
    # print(key)
    key = ''.join(key)
    # print(guessed_alphabet)
    return key, decrypt_ciphertext(ciphertext, key, ALPHABET)


# Example usage
if __name__ == "__main__":
    my_plaintext = input("Enter plaintext (enter 0 to select a file): ")
    if my_plaintext == "0":
        file_path = filedialog.askopenfilename()
        print(file_path)
        length = 0
        my_alphabet = ALPHABET
        while length == 0:
            temp_alphabet = input("Enter a custom Vigenere alphabet (or 0 for normal alphabet): ")
            if temp_alphabet == "0":
                length = len(my_alphabet)
            elif not temp_alphabet.isalpha():
                print("Custom Vigenere alphabet must be only alphabet characters")
            elif len(temp_alphabet) != 26:
                print("Custom Vigenere alphabet must be 26 characters long")
            elif len(set(temp_alphabet.upper())) != 26:
                print("Custom Vigenere alphabet must contain all letters A-Z")
            else:
                length = len(temp_alphabet)
                my_alphabet = temp_alphabet.upper()
        length = 0
        my_key = ""
        while length == 0:
            temp_key = input("Enter a key: ")
            if len(temp_key) < 3:
                print("Key must be greater than three characters!")
            elif not temp_key.isalpha():
                print("Key must be only alphabet characters!")
            else:
                length = len(temp_key)
                my_key = temp_key
        file = open(file_path, "r", encoding='utf8')
        my_plaintext = file.read()
        my_ciphertext = encrypt_plaintext(my_plaintext, my_key, my_alphabet)
        filename = os.path.split(file_path)[1]
        # print(filename[:-4])
        with open(os.path.join('ciphertexts', filename[:-4] + '-encrypted.txt'), 'w') as outfile:
            outfile.write(my_ciphertext)
        my_decrypted_plaintext = decrypt_ciphertext(my_ciphertext, my_key, my_alphabet)
        with open(os.path.join('decrypted texts', filename[:-4] + '-decrypted.txt'), 'w') as outfile:
            outfile.write(my_decrypted_plaintext)
        found_key, my_decrypted_plaintext = crack_ciphertext(my_ciphertext, my_alphabet)
        print("Key used:", found_key)
        with open(os.path.join('cracked decryptions', filename[:-4] + '-cracked.txt'), 'w') as outfile:
            outfile.write(my_decrypted_plaintext)
    else:
        length = 0
        my_alphabet = ALPHABET
        while length == 0:
            temp_alphabet = input("Enter a custom Vigenere alphabet (or 0 for normal alphabet): ")
            if temp_alphabet == "0":
                length = len(my_alphabet)
            elif not temp_alphabet.isalpha():
                print("Custom Vigenere alphabet must be only alphabet characters")
            elif len(temp_alphabet) != 26:
                print("Custom Vigenere alphabet must be 26 characters long")
            elif len(set(temp_alphabet.upper())) != 26:
                print("Custom Vigenere alphabet must contain all letters A-Z")
            else:
                length = len(temp_alphabet)
                my_alphabet = temp_alphabet.upper()
        length = 0
        my_key = ""
        while length == 0:
            temp_key = input("Enter a key: ")
            if len(temp_key) < 3:
                print("Key must be greater than three characters!")
            elif not temp_key.isalpha():
                print("Key must be only alphabet characters!")
            else:
                length = len(temp_key)
                my_key = temp_key
        print("Plaintext: ", my_plaintext)
        print("Alphabet used: ", my_alphabet)
        print("Key: ", my_key)
        my_ciphertext = encrypt_plaintext(my_plaintext, my_key, my_alphabet)
        print("Ciphertext: ", my_ciphertext)
        my_decrypted_plaintext = decrypt_ciphertext(my_ciphertext, my_key, my_alphabet)
        print("Decrypted plaintext: ", my_decrypted_plaintext)

